# lib-php-common-platform

[![Latest Stable Version](https://poser.pugx.org/dreamfactory/lib-php-common-platform/version.png)](https://packagist.org/packages/dreamfactory/lib-php-common-platform)
[![Total Downloads](https://poser.pugx.org/dreamfactory/lib-php-common-platform/downloads.png)](https://packagist.org/packages/dreamfactory/lib-php-common-platform)

This library contains common components for interacting with the DreamFactory Services Platform&trade;. It is, for all intents and purposes, the PHP SDK; and will be renamed as such, in the future.

## Installation

Add a line to your "require" section in your composer configuration:

	"require":           {
		"dreamfactory/lib-php-common-platform": "dev-master"
	}

Run a composer update:

    $ composer update
